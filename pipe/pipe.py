import os

from datadog_api_client import ApiClient, Configuration
from datadog_api_client.exceptions import OpenApiException
from datadog_api_client.v1.api.events_api import EventsApi
from datadog_api_client.v1.model.event_create_request import EventCreateRequest
import yaml

from bitbucket_pipes_toolkit import Pipe


DEFAULT_TEXT = f"%%% \n Alert sent from [Pipeline #{os.environ['BITBUCKET_BUILD_NUMBER']}](https://bitbucket.org/{os.environ['BITBUCKET_WORKSPACE']}/{os.environ['BITBUCKET_REPO_SLUG']}/pipelines/results/{os.environ['BITBUCKET_BUILD_NUMBER']}) \n %%%"

schema = {
    "API_KEY": {"required": True, "type": "string"},
    "TITLE": {"required": True, "type": "string"},
    "TEXT": {"required": False, "type": "string", "default": DEFAULT_TEXT},
    "PAYLOAD": {"required": False, "type": "dict", "empty": False},
    "EU_ENDPOINT": {"required": False, "type": "boolean", "default": False},
    "DEBUG": {"required": False, "type": "boolean", "default": False}
}


class DatadogSendEventPipe(Pipe):

    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        super().__init__(
            pipe_metadata=pipe_metadata,
            schema=schema,
            env=env,
            check_for_newer_version=check_for_newer_version
        )
        self.api_key = self.get_variable("API_KEY")
        self.title = self.get_variable("TITLE")
        self.text = self.get_variable("TEXT")
        self.payload = self.get_variable("PAYLOAD")
        # create a instance of the API class
        self.api_instance = self.from_api_key()

    def from_api_key(self):
        configuration = Configuration()
        configuration.api_key["apiKeyAuth"] = self.api_key

        if self.get_variable("EU_ENDPOINT"):
            configuration.server_variables["site"] = "datadoghq.eu"

        if self.get_variable("DEBUG"):
            configuration.debug = True

        return EventsApi(ApiClient(configuration))

    def run(self):
        self.log_info('Creating datadog event...')

        payload = self.payload or dict()
        payload.update({"title": self.title, "text": self.text})

        body = EventCreateRequest(**payload)

        try:
            response = self.api_instance.create_event(body=body)
        except OpenApiException as e:
            self.fail(f"Pipe has finished with an error: {e}")

        self.log_info(f"Datadog event sent successfully. You can check your event here: {response.event.url}")

        self.success('Pipe has finished successfully.')


if __name__ == '__main__':
    with open('/pipe.yml') as f:
        metadata = yaml.safe_load(f.read())
    pipe = DatadogSendEventPipe(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
