import io
import os
import sys
from copy import copy
from contextlib import contextmanager
from datadog_api_client.exceptions import OpenApiException
from unittest import TestCase

import pytest

from pipe.pipe import DatadogSendEventPipe, schema


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class CreateAlertCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, mocker):
        self.caplog = caplog
        self.mocker = mocker

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    def test_success(self):
        self.mocker.patch.dict(
            os.environ, {
                'API_KEY': 'test-api-key',
                'TITLE': 'test-title'
            }
        )

        api_instance_mock = self.mocker.MagicMock()
        api_instance_mock.patch('datadog_api_client.v1.api.events_api.EventsApi')

        pipe = DatadogSendEventPipe(
            schema=schema, check_for_newer_version=True)

        pipe.api_instance = api_instance_mock

        with capture_output() as out:
            pipe.run()

        self.assertIn('✔ Pipe has finished successfully.', out.getvalue())

    def test_create_failed(self):
        self.mocker.patch.dict(
            os.environ, {
                'API_KEY': 'test-api-key',
                'TITLE': 'test-title'
            }
        )

        api_instance_mock = self.mocker.Mock()
        api_instance_mock.patch('datadog_api_client.v1.api.events_api.EventsApi')
        api_instance_mock.create_event.side_effect = OpenApiException('mocked error')

        pipe = DatadogSendEventPipe(
            schema=schema, check_for_newer_version=True)

        pipe.api_instance = api_instance_mock

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                pipe.run()

        self.assertIn('Pipe has finished with an error: mocked error', out.getvalue())
        self.assertEqual(pytest_wrapped_e.type, SystemExit)

    def test_no_title_failed(self):
        self.mocker.patch.dict(
            os.environ, {
                'API_KEY': 'test-api-key'
            }
        )
        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                DatadogSendEventPipe(
                    schema=schema, check_for_newer_version=True)

        self.assertIn('TITLE:\n- required field', out.getvalue())
        self.assertEqual(pytest_wrapped_e.type, SystemExit)
