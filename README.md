# Bitbucket Pipelines Pipe: Datadog Send Event

Sends an event to [Datadog][datadog]. It allows you to programmatically post events to the [event stream][datadog event stream] and visualize and search for them in the UI.
                          
## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/datadog-send-event:2.3.1
  variables:
    API_KEY: '<string>'
    TITLE: '<string>'
    # TEXT: '<string>' # Optional.
    # PAYLOAD: '<json>' # Optional.
    # DEBUG: '<boolean>' # Optional.
    # EU_ENDPOINT: '<boolean>' # Optional.
```

## Variables

| Variable    | Usage                                                                                                                                                                                                                             |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| API_KEY (*) | API key. It is recommended to use a secure repository variable.                                                                                                                                                                   |
| TITLE (*)   | The event title. Limited to 100 characters.                                                                                                                                                                                       |
| TEXT        | The body of the event. Limited to 4000 characters. The text supports markdown. Default: `Alert sent from Pipeline #${BITBUCKET_BUILD_NUMBER}` which links to the build.                                                           |
| PAYLOAD     | JSON document containing payload supported variables for [creating event][create alert]. The value should be a dictionary(mapping) {"key": "value"}. The Examples section below contains an example for passing payload to alert. |
| EU_ENDPOINT | If `true` then use `"datadoghq.eu"` endpoint, else - `"datadoghq.com"`. Default: `false`.                                                                                                                                         |
| DEBUG       | Turn on extra debug information. Default: `false`.                                                                                                                                                                                |

_(*) = required variable._


## Prerequisites

To send events to Datadog you need an API key. You can follow the instructions [here][datadog keys].


## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/datadog-send-event:2.3.1
    variables:
      API_KEY: $API_KEY
      TITLE: 'This is the title'
```

Advanced example with payload and eu endpoint:

```yaml
script:
  - pipe: atlassian/datadog-send-event:2.3.1
    variables:
      API_KEY: $API_KEY
      TITLE: 'This is the title'
      TEXT: 'This is the description'
      PAYLOAD:  >
        {
           "priority": "low", 
           "tags": ["tag1", "tag2"]
        }
      EU_ENDPOINT: 'true'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[create-event]: https://docs.datadoghq.com/api/latest/events/#post-an-event
[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,datadog
[datadog]: https://datadoghq.com
[datadog event stream]: https://docs.datadoghq.com/graphing/event_stream/
[datadog keys]: https://docs.datadoghq.com/account_management/api-app-keys/
