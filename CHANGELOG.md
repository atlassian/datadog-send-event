# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.3.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 2.3.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 2.2.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 2.1.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerabilities with certify and gitpython.
- patch: Internal maintenance: bump pipes versions in pipelines config file.

## 2.0.2

- patch: Internal maintenance: delete unused file.

## 2.0.1

- patch: Added link to default TEXT variable.

## 2.0.0

- major: Breaking change: PRIORITY, ALERT_TYPE and TAGS are not used anymore. Now you should use PAYLOAD instead.
- patch: Update community link.
- patch: Update release process.

## 1.1.3

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.1.2

- patch: Internal maintenance: Add gitignore secrets.

## 1.1.1

- patch: Update the Readme with a new Atlassian Community link.

## 1.1.0

- minor: Add argument to choose endpoint

## 1.0.2

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 1.0.1

- patch: Refactor pipe code to use pipes bash toolkit.

## 1.0.0

- major: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.2.4

- patch: Updated contributing guidelines

## 0.2.3

- patch: Fixed the issue with doublequotes in a payload

## 0.2.2

- patch: Add default text to readme

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Adopt new naming and consistency conventions.

## 0.1.1

- patch: Improve task logging

## 0.1.0

- minor: Initial version
